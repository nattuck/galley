xs = System.argv()

if length(xs) != 0 do
  IO.puts "Usage:\nmix run scripts/regen-thumbs.exs\n"
  raise "bad usage"
end

photos = Galley.Photos.list_photos()

Enum.each photos, fn photo ->
  Galley.Photos.Photo.make_thumb!(photo)
end
