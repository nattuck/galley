#!/bin/bash
export ENV=prod
export PORT=4014
export DATABASE_PATH=/home/galley/prod.db
export SECRET_KEY_BASE=$(cat /home/galley/secret)
_build/prod/rel/galley/bin/galley start
