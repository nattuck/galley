#!/bin/bash
export MIX_ENV=prod
export PORT=4014
export DATABASE_PATH=/home/galley/prod.db
export SECRET_KEY_BASE=$(cat /home/galley/secret)

mix local.hex --force
mix local.rebar --force
mix deps.get
mix compile
mix ecto.create
mix ecto.migrate
