xs = System.argv()

if length(xs) != 1 do
  IO.puts "Usage:\nmix run scripts/gen-album.exs <album-id>\n"
  raise "bad usage"
end

xs
|> hd()
|> String.to_integer
|> Galley.GenStatic.gen_album!()
