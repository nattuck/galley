# Galley


## System Dependencies

The ```zip``` binary must be available. In Debian, that's in the ```zip```
package.

## Random Notes

Models:

 * Users
 * Photos
 * Albums
 * Uploads

## Data Storage

Photos want to be served as:

 * /g/{user-slug}/{album-slug}/images/{hash}.jpg
 * /g/{user-slug}/{album-slug}/page00.html

Plus supplemental data and deps:
 
 * /g/{user-slug}/{album-slug}/users/{user-slug}.json
 * /g/{user-slug}/{album-slug}/galley-bundle.{css|js}

Additionally, we want to be able to serve any branch of this directory tree
(e.g. an album, or all of a user's albums) as a static website. Therefore, we're
going to store our pages statically in that structure.

Styles and js should work in the same directory even when opening HTML files
via file URLs. 

Zip archives will store the contents of symlinks by default, so images, css, and
js can be linked in that way.

Aside from image files, this stuff is mostly stored in the DB.

## Default Phoenix README

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix

## License

Galley is copyright Nat Tuck and is licensed under the terms of the GNU Affero
GPL version 3 or later.


