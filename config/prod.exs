import Config

config :galley, GalleyWeb.Endpoint,
  url: [scheme: "https", host: "galley.fogcloud.org", port: 443],
  cache_static_manifest: "priv/static/cache_manifest.json"

# Do not print debug messages in production
config :logger, level: :info


