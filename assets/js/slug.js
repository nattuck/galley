import $ from 'cash-dom';

function slugify(text) {
  return text.toLowerCase().replaceAll(/[^a-z\d\-]+/g, '-');
}

function setup() {
  $('.slug-input').each((_, item) => {
    function update_slug(ev) {
      item.value = slugify(ev.target.value);
    }

    item.addEventListener('change', update_slug);
    let slugref = $(item).data('slugref');
    let input = document.getElementById(slugref);
    if (input) {
      input.addEventListener('change', update_slug);
    }
  });
}

$(setup);
