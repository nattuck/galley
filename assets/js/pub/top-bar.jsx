import React, { useState } from 'react';
import { createRoot } from 'react-dom/client';
import { Provider, useSelector } from 'react-redux';
import { BsCloudSlash } from 'react-icons/bs';
import $ from 'cash-dom';

import store from './store';
import { fetch_session } from './ajax';

function TopBar(_props) {
  const session = useSelector(({session}) => session);
  if (!session) {
    return (<BsCloudSlash />);
  }

  return (
    <p>Logged in as {session.user.name}</p>
  );
}

function init() {
  let root_div = document.getElementById('pub-top-bar');
  if (root_div) {
    fetch_session();
    console.log("top bar init");

    let root = createRoot(root_div);
    root.render(
      <React.StrictMode>
        <Provider store={store}>
          <TopBar />
        </Provider>
      </React.StrictMode>
    );
  }
}

$(init);
