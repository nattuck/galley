
import store from './store';

export async function fetch_object(path) {
  let resp = await fetch(path);
  if (resp.ok) {
    let data = await resp.json();
    return data;
  }
  else {
    return null;
  }
}

export async function post_object(path, object) {
  let resp = await fetch(path, {
    method: 'POST',
    headers: {
      'accept': 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify(object),
  });
  let data = await resp.json();
  return data;
}

export function fetch_session() {
  const path = '/ajax/session';
  fetch_object(path).then((resp) => {
    console.log("session", resp);
    if (resp) {
      store.dispatch({
        type: 'session/put',
        data: resp,
      });
    }
    else {
      store.dispatch({type: 'online/put'});
    }
  });
}

export function post_comment(comment, cb = null) {
  let path = '/ajax/comments';
  post_object(path, {comment: comment}).then((resp) => {
    console.log("comment", resp);
    store.dispatch({
      type: 'comment/put',
      data: resp.data,
    });
    if (cb) {
      cb(resp);
    }
  });
}
