import React, { useState } from 'react';
import { createRoot } from 'react-dom/client';
import { Provider, useSelector } from 'react-redux';
import { BsCloudSlash, BsFileEarmarkZip } from 'react-icons/bs';
import $ from 'cash-dom';

import store from './store';

function ZipLink({album}) {
  const {online} = useSelector((xx) => xx);

  if (online) {
    let path = `../${album.slug}.zip`;
    return (
      <small>
        <a href={path}>
          <BsFileEarmarkZip />
        </a>
      </small>
    );
  }
  else {
    return (
      <BsCloudSlash />
    );
  }
}

function init() {
  let root_div = document.getElementById('pub-zip-link');
  let album = window.album;
  if (root_div && album) {
    let root = createRoot(root_div);
    root.render(
      <React.StrictMode>
        <Provider store={store}>
          <ZipLink album={album} />
        </Provider>
      </React.StrictMode>
    );
  }
}

$(init);
