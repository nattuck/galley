
import { createStore, combineReducers } from 'redux';

function online(state = false, action) {
  switch(action.type) {
    case 'session/put':
      return true;
    case 'online/put':
      return true;
    default:
      return state;
  }
}

function session(state = null, action) {
  switch (action.type) {
    case 'session/put':
      return action.data;
    default:
      return state;
  }
}

function comments(state = [], action) {
  switch (action.type) {
    case 'comment/put':
      return state.concat(action.data);
    default:
      return state;
  }
}

function root_reducer(state, action) {
  let reducer = combineReducers({comments, online, session});
  let state1 = reducer(state, action);
  console.log(action, state1);
  return state1;
}

let store = createStore(root_reducer);
export default store;
