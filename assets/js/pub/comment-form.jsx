import React, { useState } from 'react';
import { createRoot } from 'react-dom/client';
import { Provider, useSelector } from 'react-redux';
import { Button, Card, Form } from 'react-bootstrap';
import { BsCloudSlash } from 'react-icons/bs';
import $ from 'cash-dom';

import store from './store';
import { post_comment } from './ajax';

function CommentForm({photo}) {
  const {comments, session} = useSelector((xx) => xx);

  if (!session) {
    return (<BsCloudSlash />);
  }

  return (
    <div>
      <Comments comments={comments} />
      <TheForm photo={photo} user={session.user} />
    </div>
  );
}

function Comments({comments}) {
  if (comments.length == 0) {
    return null;
  }

  let xs = comments.map((cc) => (
    <Card key={cc.id} className="my-1">
      <Card.Body>
        <p>
          <strong>{ cc.user.name }</strong>
          &nbsp;on&nbsp;
          { cc.date }
        </p>
        <Card.Text>{ cc.body } </Card.Text>
      </Card.Body>
    </Card>
  ));

  return (
    <>
      <h3>Pending Comments</h3>
      { xs }
    </>
  );
}

function TheForm({photo, user}) {
  const [body, setBody] = useState("");
  const [enab, setEnab] = useState(true);

  function submit() {
    let photo_id = photo.id;
    let comment = {body, photo_id};
    setEnab(false);
    post_comment(comment, () => {
      setEnab(true);
      setBody("");
    });
  }

  return (
    <Form onSubmit={submit}>
      <h3 className="my-2">Add Comment</h3>
      <p>Posting as {user.name}</p>
      <Form.Group className="mb-3">
        <Form.Control as="textarea"
                      value={body}
                      onChange={(ev) => setBody(ev.target.value)}
                      rows={3} />
      </Form.Group>
      <Form.Group className="mb-3">
        <Button variant="primary" onClick={submit} disabled={!enab}>
          Post
        </Button>
      </Form.Group>
    </Form>
  );
}

function init() {
  let root_div = document.getElementById('pub-comment-form');
  if (root_div && window.photo) {
    let root = createRoot(root_div);
    root.render(
      <React.StrictMode>
        <Provider store={store}>
          <CommentForm photo={window.photo}/>
        </Provider>
      </React.StrictMode>
    );
  }
}

$(init);
