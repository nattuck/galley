
import axios from 'axios';
import { Sha256 } from  '@aws-crypto/sha256-browser';
import to_hex from 'array-buffer-to-hex';

import { push_msg } from './channel';

const base_path = '/ajax/upload';

async function hash_file(file) {
  let data = await file.arrayBuffer();
  let hasher = new Sha256();
  hasher.update(data);
  let hash = await hasher.digest();
  return to_hex(hash).slice(0, 48);
}

export async function start_uploads(files, progress_cb) {
  let by_hash = new Map();

  let photos = [];
  for (let file of files) {
    let { name, size, type } = file;
    let photo = { name, size, type };
    progress_cb(photo, null);

    let hash = await hash_file(file);
    photo.hash = hash;
    progress_cb(photo, null);

    by_hash.set(hash, file);
    photos.push(photo);
  }

  let resp1 = await push_msg("photos_start", {photos});
  console.log("starting", resp1);

  let uploads = [];
  for (let photo of resp1.photos) {
    let up = upload(photo, by_hash.get(photo.hash), progress_cb)
    uploads.push(up);
  }
  photos = await Promise.all(uploads);

  let resp2 = await push_msg("photos_done", {photos});
  console.log("done", resp2);

  for (let photo of resp2.photos) {
    progress_cb(photo, null);
  }
}

async function upload(photo, file, progress_cb) {
  let path = base_path + "/" + photo.hash;
  //console.log("path", path);

  let body = new FormData();
  body.append('file', file);
  body.append('name', photo.name);
  body.append('size', photo.size);
  body.append('type', photo.type);

  let resp = await axios.post(path, body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    maxRedirects: 0,
    onUploadProgress: (event) => progress_cb(photo, event),
  });

  console.log("resp", resp);
  return resp.data;
}
