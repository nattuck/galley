
import { clamp } from 'lodash';
import { Buffer } from 'buffer';

import socket from '../user_socket';
import store from './store';

export let channel = null;
if (window.album_id) {
  channel = socket.channel("album:" + window.album_id || "none", {});
  channel.join()
         .receive("ok", resp => { console.log("Joined successfully", resp) })
         .receive("error", resp => { console.log("Unable to join", resp) });
}

export async function push_msg(tag, msg) {
  return new Promise((resolve, reject) => {
    channel.push(tag, msg)
           .receive("ok", resolve)
           .receive("error", reject);
  });
}

