
import { createStore, combineReducers } from 'redux';

function put_photo(xs, photo) {
  let seen = false;
  let ys = xs.map((xx) => {
    if (xx.hash == photo.hash) {
      seen = true;
      return photo;
    }
    else if (xx.name == photo.name) {
      seen = true;
      return photo;
    }
    else {
      return xx;
    }
  });
  if (!seen) {
    ys.push(photo);
  }
  return ys;
}

function uploads(state = [], action) {
  let ph = null;
  switch (action.type) {
    case 'uploads/put':
      return put_photo(state, action.photo);
    default:
      return state;
  }
}

function album(state = null, action) {
  switch (action.type) {
    case 'album/put':
      return action.album;
    case 'photos/add':
      let album = Object.assign({}, state);
      let photo = action.photo;
      album.photos = (album.photos || [])
        .filter((ph) => ph.hash != photo.hash)
        .concat(photo);
      return album;
    default:
      return state;
  }
}

function root_reducer(state, action) {
  let reducer = combineReducers({album, uploads});
  let state1 = reducer(state, action);
  console.log(action, state1);
  return state1;
}

let store = createStore(root_reducer);
export default store;
