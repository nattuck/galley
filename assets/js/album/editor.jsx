import React, { useState } from 'react';
import { createRoot } from 'react-dom/client';
import { Provider, useSelector } from 'react-redux';
import $ from 'cash-dom';
import { clone, sortBy } from 'lodash';
import { Button, Card, Form, Container, Col, Row } from 'react-bootstrap';

import { channel } from './channel';
import store from './store';

function AlbumEditor(_props) {
  const album = useSelector(({album}) => album);

  if (!album) {
    return (
      <div>
        <p>Loading...</p>
      </div>
    );
  }

  //console.log("photos", album.photos);

  function setPhoto(photo) {
    let album1 = clone(album);
    album1.photos = album.photos.map((ph) => {
      if (ph.id == photo.id) {
        return photo;
      }
      else {
        return ph;
      }
    });
    store.dispatch({
      type: 'album/put',
      album: album1,
    });
  }

  function dropPhoto(photo) {
    let album1 = clone(album);
    album1.photos = album.photos.filter((ph) => (
      ph.id != photo.id
    ));
    store.dispatch({
      type: 'album/put',
      album: album1,
    });
  }

  function update_photo(photo0, attrs) {
    return () => {
      let photo = Object.assign({}, photo0, attrs);
      channel.push("update_photo", {photo})
             .receive("ok", setPhoto);
    }
  }

  function delete_photo(photo) {
    return () => {
      if (confirm("Are you sure?")) {
        channel.push("delete_photo", {photo})
               .receive("ok", dropPhoto);
      }
    }
  }

  function shift_order(photo, dx) {
    return () => {
      channel.push("shift_photo", {photo, dx})
             .receive("ok", (album) => {
               store.dispatch({
                 type: 'album/put',
                 album: album,
               });
             });
    }
  }

  let cards = sortBy(album.photos, (xx) => xx.order).map((ph, ii) => (
    <PhotoCard photo={ph}
               update_photo={update_photo}
               delete_photo={delete_photo}
               shift_order={shift_order}
               key={[ph.id, ii]} />
  ));

  return (
    <Row>
      { cards }
    </Row>
  );
}

function PhotoCard({photo, update_photo, delete_photo, shift_order}) {
  return (
    <Col sm="3">
      <Card>
        <Card.Img variant="top" src={photo.thumb} />
        <Card.Body>
          <SwapButtons photo={photo} shift_order={shift_order} />
          <div className="my-2">
            <p>{photo.filename} ({size(photo.bytes)})</p>
          </div>
          <div>
            <a href={"/photos/" + photo.id}>
              id: {photo.id}; order: {photo.order}
            </a>
          </div>
          <PhotoForm photo={photo}
                     update_photo={update_photo}
                     delete_photo={delete_photo} />
        </Card.Body>
      </Card>
    </Col>
  );
}

function SwapButtons({photo, shift_order}) {
  return (
    <div className="my-2">
      <p>Swap:&nbsp;
        <Button variant="secondary"
                size="sm"
                onClick={shift_order(photo, -1)}>
          &larr;
        </Button>&nbsp;
        <Button variant="secondary"
                size="sm"
                onClick={shift_order(photo, 1)}>
          &rarr;
        </Button>
      </p>
    </div>
  );
}

function PhotoForm({photo, update_photo, delete_photo}) {
  const [order, setOrder] = useState(photo.order);
  const [caption, setCaption] = useState(photo.caption);

  return (
    <>
      <Form.Group className="my-2">
        <Form.Label>Caption</Form.Label>
        <Form.Control as="textarea"
                      value={caption}
                      onChange={(ev) => setCaption(ev.target.value)} />
      </Form.Group>
      <Form.Group className="my-2">
        <Form.Label>Sort Order</Form.Label>
        <Form.Control value={order}
                      onChange={(ev) => setOrder(ev.target.value)}
                      size="sm"
                      type="number" />
      </Form.Group>
      <div className="my-2">
        <Button onClick={update_photo(photo, {order, caption})}
                disabled={photo.order == order && photo.caption == caption}
                variant="primary">
          Update
        </Button>
        <Button onClick={delete_photo(photo)}
                variant="danger">
          Delete
        </Button>
      </div>
    </>
  );
}

function size(xx) {
  const units = ["B", "kB", "MB", "GB", "TB"];
  let zz = 0;
  while (xx > 1024) {
    zz += 1;
    xx = Math.round(xx / 1024);
  }
  let unit = units[zz] || "??";
  return `${xx} ${unit}`;
}

function init() {
  let root_div = document.getElementById('album-editor');
  if (root_div && window.album) {
    store.dispatch({
      type: 'album/put',
      album: window.album,
    });
    let root = createRoot(root_div);
    root.render(
      <React.StrictMode>
        <Provider store={store}>
          <AlbumEditor />
        </Provider>
      </React.StrictMode>
    );
  }
}

$(init);

