import React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider, useSelector } from 'react-redux';
import Dropzone from 'react-dropzone';
import { Button, Card, Row, Col, Table } from 'react-bootstrap';
import { BsCloudUpload, BsCheckCircle } from 'react-icons/bs';
import $ from 'cash-dom';
import { uniqBy } from 'lodash';

import { start_uploads } from './upload';
import store from './store';

function BulkUploader(_props) {
  return (
    <div>
      <DropRow />
      <ProgressRow />
    </div>
  );
}

function DropRow(_props) {
  function progress_cb(photo, event) {
    let pct = 0;
    if (photo.id) {
      pct = 100;

      store.dispatch({
        type: "photos/add",
        photo: photo,
      });
    }
    if (event) {
      pct = Math.round(100 * event.loaded / event.total);
    }
    photo.pct = pct;

    store.dispatch({
      type: "uploads/put",
      photo: photo,
    });
  }

  function drop(files) {
    start_uploads(files, progress_cb);
  }

  return (
    <Row>
      <Col>
        <Dropzone onDrop={drop}>
          {({getRootProps, getInputProps}) => (
            <div {...getRootProps()}>
              <Card>
                <Card.Body className="text-center">
                  <input {...getInputProps()} />
                  <p>Drag and drop files here.</p>
                  <p><BsCloudUpload /></p>
                  <Button>Browse...</Button>
                </Card.Body>
              </Card>
            </div>
          )}
        </Dropzone>
      </Col>
    </Row>
  );
}

function ProgressRow(_props) {
  let photos = useSelector(({uploads}) => uploads);
  photos = uniqBy(photos, (ph) => ph.name + ph.hash);

  function photo2row(photo) {
    let note = null;
    if (photo.pct == 100) {
      note = <BsCheckCircle />;
    }
    if (photo.errors && photo.errors.length > 0) {
      note = (
        <span className="upload-error">{photo.errors[0]}</span>
      );
    }

    return (
      <tr key={photo.name + photo.hash}>
        <td>{photo.name || photo.filename}</td>
        <td>{photo.pct}%</td>
        <td>{note}</td>
      </tr>
    );
  }

  let xs = photos.filter((ph) => ph.pct != 100);
  let ys = photos.filter((ph) => ph.pct == 100);

  let rows = xs.map(photo2row).concat(ys.map(photo2row));

  return (
    <Row>
      <Col>
        <Table striped bordered>
          <tbody>
            {rows}
          </tbody>
        </Table>
      </Col>
    </Row>
  );
}

function init() {
  let root_div = document.getElementById('bulk-uploader');
  if (root_div) {
    let root = createRoot(root_div);
    root.render(
      <React.StrictMode>
        <Provider store={store}>
          <BulkUploader />
        </Provider>
      </React.StrictMode>
    );
  }
}

$(init);
