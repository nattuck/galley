defmodule GalleyWeb.AlbumChannel do
  use GalleyWeb, :channel

  alias Galley.Albums
  alias Galley.Photos
  alias Galley.GenStatic
  alias Galley.Photos.UploadServer
  alias GalleyWeb.PhotoView
  alias GalleyWeb.AlbumView

  @impl true
  def join("album:" <> id_text, _payload, socket) do
    album_id = String.to_integer(id_text)
    if authorized?(album_id, socket.assigns[:user_id]) do
      socket = assign(socket, :album_id, album_id)
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  @impl true
  def handle_in("update_photo", %{"photo" => attrs}, socket) do
    {:ok, photo} = Photos.update_photo(attrs)
    {:reply, {:ok, PhotoView.to_map(photo)}, socket}
  end

  def handle_in("delete_photo", %{"photo" => attrs}, socket) do
    photo = Photos.get_photo!(attrs["id"])
    {:ok, photo} = Photos.delete_photo(photo)
    {:reply, {:ok, PhotoView.to_map(photo)}, socket}
  end

  def handle_in("shift_photo", %{"photo" => attrs, "dx" => dx}, socket) do
    IO.inspect({attrs, dx})
    {:ok, album} = Albums.shift_album_photo(attrs["album_id"], attrs["id"], dx)
    GenStatic.schedule_gen_album(album.id)
    {:reply, {:ok, AlbumView.to_map(album)}, socket}
  end

  def handle_in("photos_start", %{"photos" => photos}, socket) do
    album_id = socket.assigns[:album_id]
    order_base = Galley.Albums.next_album_order(album_id)

    photos = Enum.map Enum.with_index(photos), fn {photo, ii} ->
      photo = Map.put(photo, "order", order_base + 10*ii)
      :ok = UploadServer.put(photo["hash"], photo)
      photo
    end

    {:reply, {:ok, %{photos: photos}}, socket}
  end

  def handle_in("photos_done", %{"photos" => photos}, socket) do
    album_id = socket.assigns[:album_id]

    {:ok, photos} = Galley.Repo.transaction fn ->
      Enum.map photos, fn photo ->
        photo = photo
        |> Map.put("bytes", photo["size"])
        |> Map.put("filename", photo["name"])

        case create_photo(album_id, photo) do
          {:ok, photo} -> photo
          {:error, errors} -> Map.put(photo, "errors", errors)
        end
      end
    end

    GenStatic.schedule_gen_album(album_id)
    {:reply, {:ok, %{photos: photos}}, socket}
  end

  defp authorized?(album_id, user_id) do
    album = Galley.Albums.get_album!(album_id)
    album.user_id == user_id
  end

  def create_photo(album_id, photo) do
    photo = Map.put(photo, "album_id", album_id)

    case Galley.Photos.create_photo(photo) do
      {:ok, item} ->
        :ok = UploadServer.del(photo["hash"])
        view = PhotoView.to_map(item)
        {:ok, view}
      {:error, %Ecto.Changeset{errors: errors}} ->
        :ok = UploadServer.del(photo["hash"])
        {:error, PhotoView.show_errors(errors)}
    end
  end

  def inspect_photos(tag, sock) do
    xs = sock.assigns[:uploads]
    Enum.each xs, fn photo ->
      IO.inspect {:photo, tag, photo.uuid, photo.name}
    end
  end
end
