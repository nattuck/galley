defmodule GalleyWeb.UserView do
  use GalleyWeb, :view
  alias __MODULE__

  alias Galley.Users.User

  def to_map(%User{} = user) do
    %{
      name: user.name,
      slug: user.slug,
    }
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      name: user.name,
    }
  end
end
