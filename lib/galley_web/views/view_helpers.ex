defmodule GalleyWeb.ViewHelpers do
  use Phoenix.HTML
  #alias SnekinfoWeb.Router.Helpers, as: Routes
  #import Phoenix.View

  alias Galley.Albums
  alias Galley.Albums.Album

  def admin_session?(conn) do
    user = conn.assigns[:current_user]
    user && user.admin
  end

  def user_session?(conn) do
    !is_nil(conn.assigns[:current_user])
  end

  def norm_path(xs) when is_list(xs) do
    Enum.filter(xs, &(&1 =~ ~r/\w/))
  end
  def norm_path(text) do
    norm_path(Path.split(text))
  end

  def same_path?(aa, bb) do
    norm_path(aa) == norm_path(bb)
  end

  def nav_link(conn, text, path, opts) do
    opts = Keyword.put(opts, :to, path)
    if same_path?(conn.request_path, path) do
      opts = Keyword.merge opts, [
        class: "nav-link active",
        "aria-current": "page",
      ]
      link(text, opts)
    else
      opts = Keyword.merge opts, [class: "nav-link"]
      link(text, opts)
    end
  end

  def nav_item(conn, text, path, opts) do
    link = nav_link(conn, text, path, opts)
    content_tag("li", link, class: "nav-item")
  end
  def nav_item(conn, text, path) do
    nav_item(conn, text, path, [])
  end

  def show_date(date) do
    date
    |> LocalTime.from!()
    |> Calendar.strftime("%Y-%b-%d %H:%M %Z")
  end

  def static_show_path(photo) do
    Galley.Photos.Photo.photo_page_name(photo)
  end

  def static_index_path(album) do
    album.slug <> "/"
  end

  def static_photo_path(photo) do
    "images/" <> Galley.Photos.Photo.photo_name(photo)
  end

  def static_thumb_path(photo) do
    "thumbs/" <> Galley.Photos.Photo.photo_name(photo)
  end

  def static_user_thumb_path(album, photo) do
    Path.join([album.slug, "thumbs", Galley.Photos.Photo.photo_name(photo)])
  end

  def format_file_size(xx) do
    exp = floor(Math.log(xx, 1024))
    sig = floor(xx / Math.pow(1024, exp))
    unit = Enum.at(["B", "kB", "MB", "GB", "TB"], exp)
    "#{sig} #{unit}"
  end

  def pub_path(_conn, %Album{} = album) do
    album = Albums.load_user(album)
    Path.join(["/g", album.user.slug, album.slug])
  end

  def show_markdown(md) do
    case Earmark.as_html(md) do
      {:ok, html, _} -> raw(html)
      {:error, html, errors} -> raw """
        #{html}
        <pre>#{inspect(errors)}</pre>
      """
    end
  end

  def download_icon(size \\ 16) do
    # https://icons.getbootstrap.com/icons/download/
    raw """
    <svg xmlns="http://www.w3.org/2000/svg" width="#{size}" height="#{size}" fill="currentColor" class="bi bi-download" viewBox="0 0 16 16">
      <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
      <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
    </svg>
    """
  end

  def zipfile_icon(size \\ 16) do
    # https://icons.getbootstrap.com/icons/file-earmark-zip/
    raw """
    <svg xmlns="http://www.w3.org/2000/svg" width="#{size}" height="#{size}" fill="currentColor" class="bi bi-file-earmark-zip" viewBox="0 0 16 16">
      <path d="M5 7.5a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v.938l.4 1.599a1 1 0 0 1-.416 1.074l-.93.62a1 1 0 0 1-1.11 0l-.929-.62a1 1 0 0 1-.415-1.074L5 8.438V7.5zm2 0H6v.938a1 1 0 0 1-.03.243l-.4 1.598.93.62.929-.62-.4-1.598A1 1 0 0 1 7 8.438V7.5z"/>
      <path d="M14 4.5V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h5.5L14 4.5zm-3 0A1.5 1.5 0 0 1 9.5 3V1h-2v1h-1v1h1v1h-1v1h1v1H6V5H5V4h1V3H5V2h1V1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h-2z"/>
    </svg>
    """
  end

  def offline_icon(size \\ 16) do
    # https://icons.getbootstrap.com/icons/cloud-slash-fill/
    raw """
    <svg xmlns="http://www.w3.org/2000/svg" width="#{size}" height="#{size}" fill="currentColor" class="bi bi-cloud-slash-fill" viewBox="0 0 16 16">
      <path fill-rule="evenodd" d="M3.112 5.112a3.125 3.125 0 0 0-.17.613C1.266 6.095 0 7.555 0 9.318 0 11.366 1.708 13 3.781 13H11L3.112 5.112zm11.372 7.372L4.937 2.937A5.512 5.512 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773a3.2 3.2 0 0 1-1.516 2.711zm-.838 1.87-12-12 .708-.708 12 12-.707.707z"/>
    </svg>
    """
  end

  def circleleft_icon(size \\ 16) do
    # https://icons.getbootstrap.com/icons/arrow-left-circle/
    raw """
    <svg xmlns="http://www.w3.org/2000/svg" width="#{size}" height="#{size}" fill="currentColor" class="bi bi-arrow-left-circle" viewBox="0 0 16 16">
      <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/>
    </svg>
    """
  end
end
