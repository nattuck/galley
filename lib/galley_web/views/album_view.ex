defmodule GalleyWeb.AlbumView do
  use GalleyWeb, :view

  alias Galley.Albums.Album
  alias GalleyWeb.PhotoView

  def to_map(%Album{} = album) do
    album_photos_map = Enum.map album.photos, fn ap ->
      PhotoView.to_map(ap)
    end

    %{
      name: album.name,
      desc: album.desc,
      slug: album.slug,
      user: GalleyWeb.UserView.to_map(album.user),
      photos: album_photos_map,
    }
  end
end
