defmodule GalleyWeb.CommentView do
  use GalleyWeb, :view
  alias __MODULE__

  def render("index.json", %{comments: comments}) do
    %{data: render_many(comments, CommentView, "comment.json")}
  end

  def render("show.json", %{comment: comment}) do
    %{data: render_one(comment, CommentView, "comment.json")}
  end

  def render("comment.json", %{comment: comment}) do
    %{
      id: comment.id,
      body: comment.body,
      user_id: comment.user_id,
      photo_id: comment.photo_id,
      date: show_date(comment.updated_at),
      user: GalleyWeb.UserView.to_map(comment.user),
    }
  end
end
