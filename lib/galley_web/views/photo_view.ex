defmodule GalleyWeb.PhotoView do
  use GalleyWeb, :view

  alias Galley.Photos.Photo

  def to_map(%Photo{} = photo) do
    %{
      id: photo.id,
      album_id: photo.album_id,
      caption: photo.caption,
      order: photo.order,
      bytes: photo.bytes,
      size: photo.bytes,
      filename: photo.filename,
      hash: photo.hash,
      path: Routes.photo_path(GalleyWeb.Endpoint, :raw, photo),
      thumb: Routes.photo_path(GalleyWeb.Endpoint, :thumb, photo),
    }
  end

  def show_errors(xs) do
    Enum.map xs, fn {key, {msg, _}} ->
      "#{key} #{msg}"
    end
  end
end
