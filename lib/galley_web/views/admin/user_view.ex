defmodule GalleyWeb.Admin.UserView do
  use GalleyWeb, :view

  alias Galley.Users.User

  def to_map(%User{} = user) do
    %{
      name: user.name,
      slug: user.slug,
    }
  end
end
