defmodule GalleyWeb.AssignPlug do
  import Plug.Conn

  def init(opts) do
    opts
  end

  def call(conn, opts) do
    Enum.reduce opts, conn, fn {kk, vv}, acc ->
      assign(acc, kk, vv)
    end
  end
end
