defmodule GalleyWeb.PutUserToken do
  import Plug.Conn

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    if current_user = conn.assigns[:current_user] do
      token = Phoenix.Token.sign(conn, "user socket", current_user.id)
      assign(conn, :user_token, token)
    else
      conn
    end
  end
end
