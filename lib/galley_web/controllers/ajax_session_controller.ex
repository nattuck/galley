defmodule GalleyWeb.AjaxSessionController do
  use GalleyWeb, :controller

  alias GalleyWeb.UserView

  def show(conn, _params) do
    user = UserView.to_map(conn.assigns[:current_user])
    body = Jason.encode!(%{user: user})

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, body)
  end
end
