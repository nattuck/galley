defmodule GalleyWeb.UploadController do
  use GalleyWeb, :controller

  alias Galley.Photos.Photo
  alias Galley.Photos.UploadServer

  def upload(conn, %{"hash" => hash, "file" => file}) do
    if {:ok, meta} = UploadServer.get(hash) do
      path = Photo.photo_path(hash)
      File.cp!(file.path, path)
      Photo.make_thumb!(path)

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(201, Jason.encode!(meta))
    else
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(403, %{reason: "Must preauthorize upload"})
    end
  end
end
