defmodule GalleyWeb.AlbumController do
  use GalleyWeb, :controller

  alias Galley.Albums
  alias Galley.Albums.Album
  alias Galley.GenStatic
  alias GalleyWeb.AlbumView

  def index(conn, _params) do
    user = conn.assigns[:current_user]
    albums = if user.admin do
      Albums.list_albums()
    else
      Albums.list_albums_for_user(user)
    end
    render(conn, "index.html", albums: albums)
  end

  def new(conn, _params) do
    changeset = Albums.change_album(%Album{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"album" => album_params}) do
    album_params = album_params
    |> Map.put("user_id", conn.assigns[:current_user].id)

    case Albums.create_album(album_params) do
      {:ok, album} ->
        GenStatic.schedule_gen_album(album.id)

        conn
        |> put_flash(:info, "Album created successfully.")
        |> redirect(to: Routes.album_path(conn, :show, album))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    album = Albums.get_album_and_contents!(id)
    album_json = Jason.encode!(AlbumView.to_map(album), pretty: true)
    render(conn, "show.html", album: album, album_json: album_json)
  end

  def edit(conn, %{"id" => id}) do
    album = Albums.get_album!(id)
    changeset = Albums.change_album(album)
    render(conn, "edit.html", album: album, changeset: changeset)
  end

  def update(conn, %{"id" => id, "album" => album_params}) do
    album = Albums.get_album!(id)

    case Albums.update_album(album, album_params) do
      {:ok, album} ->
        GenStatic.schedule_gen_album(album.id)

        conn
        |> put_flash(:info, "Album updated successfully.")
        |> redirect(to: Routes.album_path(conn, :show, album))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", album: album, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    album = Albums.get_album!(id)
    {:ok, _album} = Albums.delete_album(album)

    conn
    |> put_flash(:info, "Album deleted successfully.")
    |> redirect(to: Routes.album_path(conn, :index))
  end
end
