defmodule GalleyWeb.PageController do
  use GalleyWeb, :controller

  alias Galley.Users

  def index(conn, _params) do
    users = Users.list_users_for_landing()

    render(conn, "index.html", users: users)
  end
end
