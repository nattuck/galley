defmodule GalleyWeb.Pub.PubController do
  use GalleyWeb, :controller

  #alias Galley.Albums
  alias Galley.GenStatic

  def serve(conn, path) do
    filename = Path.basename(path)
    cdisp = "inline; filename=\"#{filename}\""

    if File.exists?(path) do
      conn
      |> put_resp_header("x-content-type-options", "nosniff")
      |> put_resp_header("content-disposition", cdisp)
      |> put_resp_content_type(guess_content_type(path))
      |> send_file(200, path)
    else
      IO.inspect({:show404, path})
      conn
      |> send_resp(404, "not found")
    end
  end

  def user(conn, ~m{user}) do
    path = GenStatic.user_base_path(user)
    |> Path.join("index.html")
    serve(conn, path)
  end

  def index(conn, ~m{user, album}) do
    cond do
      Regex.match?(~r/\.(css|js)$/, conn.request_path) ->
        path = GenStatic.user_base_path(user)
        |> Path.join(album)
        serve(conn, path)
      Regex.match?(~r/\.zip$/, conn.request_path) ->
        zip(conn, %{"user" => user, "album" => album})
      Regex.match?(~r/\/$/, conn.request_path) ->
        show(conn,  %{"user" => user, "album" => album, "rest" => ["index.html"]})
      true ->
        # Need trailing slash for relative paths in static HTML to work.
        redirect(conn, to: conn.request_path <> "/")
    end
  end

  def show(conn, ~m{user, album, rest}) do
    path = GenStatic.path_from_slugs(user, album)
    |> Path.join(Path.join(rest))

    serve(conn, path)
  end

  def zip(conn, ~m{user, album}) do
    path = GenStatic.path_from_slugs(user, album)

    if File.exists?(path) do
      filename = Path.basename(path)
      cdisp = "attachment; filename=\"#{filename}\""
      conn
      |> put_resp_header("x-content-type-options", "nosniff")
      |> put_resp_content_type("application/zip")
      |> put_resp_header("content-disposition", cdisp)
      |> send_file(200, path)
    else
      IO.inspect({:zip404, path})
      conn
      |> send_resp(404, "not found")
    end
  end

  def guess_content_type(path) when is_binary(path) do
    Regex.run(~r/\.\w+$/, path)
    |> ext_to_ctype()
  end
  def guess_content_type(path) when is_list(path) do
    Path.join(path)
    |> guess_content_type()
  end

  def ext_to_ctype(nil), do: "text/plain"
  def ext_to_ctype([ext]) do
    case ext do
      ".html" -> "text/html"
      ".jpg" -> "image/jpeg"
      ".js" -> "application/javascript"
      ".css" -> "text/css"
      _else -> "text/plain"
    end
  end
end
