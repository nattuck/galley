defmodule GalleyWeb.PhotoController do
  use GalleyWeb, :controller

  alias Galley.Photos
  alias Galley.Photos.Photo
  alias Galley.Albums
  alias Galley.GenStatic

  def index(conn, %{"album_id" => album_id}) do
    album = Albums.get_album!(album_id)
    photos = Photos.list_photos(album.id)
    render(conn, "index.html", album: album, photos: photos)
  end

  def bulk_upload(conn, %{"album_id" => album_id}) do
    album = Albums.get_album!(album_id)
    render(conn, "bulk_upload.html", album: album)
  end

  def new(conn, %{"album_id" => album_id}) do
    album = Albums.get_album!(album_id)
    order = Albums.next_album_order(album)
    default = %Photo{ album_id: album_id, order: order }
    changeset = Photos.change_photo(default)
    action = Routes.album_photo_path(conn, :create, album)
    render(conn, "new.html", album: album, changeset: changeset, action: action)
  end

  def create(conn, %{"album_id" => album_id, "photo" => photo_params}) do
    album = Albums.get_album!(album_id)
    photo_params = photo_params
    |> Map.put("album_id", album.id)

    case Photos.create_photo(photo_params) do
      {:ok, photo} ->
        GenStatic.schedule_gen_album(photo.album_id)

        conn
        |> put_flash(:info, "Photo created successfully.")
        |> redirect(to: Routes.album_path(conn, :show, photo.album_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        action = Routes.album_photo_path(conn, :create, album)
        render(conn, "new.html", album: album, changeset: changeset,
          action: action)
    end
  end

  def show(conn, %{"id" => id}) do
    photo = Photos.get_photo!(id)
    render(conn, "show.html", photo: photo)
  end

  def raw(conn, %{"id" => id}) do
    photo = Photos.get_photo!(id)
    path = Photo.photo_path(photo)
    cdisp = "inline; filename=\"#{photo.filename}\""

    conn
    |> put_resp_header("content-disposition", cdisp)
    |> put_resp_content_type("image/jpeg")
    |> send_file(200, path)
  end

  def thumb(conn, %{"id" => id}) do
    photo = Photos.get_photo!(id)
    path = Photo.thumb_path(photo)
    cdisp = "inline; filename=\"thumb-#{photo.filename}\""

    conn
    |> put_resp_header("content-disposition", cdisp)
    |> put_resp_content_type("image/jpeg")
    |> send_file(200, path)
  end

  def edit(conn, %{"id" => id}) do
    photo = Photos.get_photo!(id)
    album = Albums.get_album!(photo.album_id)
    action = Routes.photo_path(conn, :update, photo)

    changeset = Photos.change_photo(photo)
    render(conn, "edit.html", album: album, photo: photo,
      action: action, changeset: changeset)
  end

  def update(conn, %{"id" => id, "photo" => photo_params}) do
    photo = Photos.get_photo!(id)
    album = Albums.get_album!(photo.album_id)
    action = Routes.photo_path(conn, :update, photo)

    case Photos.update_photo(photo, photo_params) do
      {:ok, photo} ->
        GenStatic.schedule_gen_album(photo.album_id)

        conn
        |> put_flash(:info, "Photo updated successfully.")
        |> redirect(to: Routes.photo_path(conn, :show, photo))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", album: album, photo: photo,
          action: action, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    photo = Photos.get_photo!(id)
    {:ok, _photo} = Photos.delete_photo(photo)

    conn
    |> put_flash(:info, "Photo deleted successfully.")
    |> redirect(to: Routes.album_photo_path(conn, :index, photo.album_id))
  end
end
