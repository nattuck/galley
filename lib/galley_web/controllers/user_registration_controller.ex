defmodule GalleyWeb.UserRegistrationController do
  use GalleyWeb, :controller

  alias Galley.Users
  alias Galley.Users.User
  alias Galley.Invites
  alias GalleyWeb.UserAuth

  def new(conn, %{"email" => email}) do
    if Invites.invite_exists?(email) do
      changeset = Users.change_user_registration(%User{email: email})
      render(conn, "new.html", changeset: changeset, email: email)
    else
      conn
      |> put_flash(:error, "No invite for #{email}")
      |> redirect(to: Routes.user_registration_path(conn, :new))
    end
  end

  def new(conn, _params) do
    render(conn, "email.html")
  end

  def create(conn, %{"user" => user_params}) do
    email =user_params["email"]
    unless Invites.invite_exists?(email) do
      raise "No invite for email"
    end

    case Users.register_user(user_params) do
      {:ok, user} ->
        {:ok, _} =
          Users.deliver_user_confirmation_instructions(
            user,
            &Routes.user_confirmation_url(conn, :edit, &1)
          )

        Users.make_one_admin()

        conn
        |> put_flash(:info, "User created successfully.")
        |> UserAuth.log_in_user(user)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, email: email)
    end
  end
end
