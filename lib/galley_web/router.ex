defmodule GalleyWeb.Router do
  use GalleyWeb, :router

  import Phoenix.LiveDashboard.Router
  import GalleyWeb.UserAuth

  pipeline :static do
    plug :accepts, ["html"]
    plug :put_secure_browser_headers
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {GalleyWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
    plug GalleyWeb.PutUserToken
  end

  pipeline :ajax do
    plug :accepts, ["json"]
    plug GalleyWeb.AssignPlug, ajax: true
    plug :fetch_session
    plug :fetch_flash
    plug :put_secure_browser_headers
    plug :fetch_current_user
    plug GalleyWeb.PutUserToken
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/g", GalleyWeb.Pub do
    pipe_through :static

    get "/:user", PubController, :user
    get "/:user/:album", PubController, :index
    get "/:user/:album/*rest", PubController, :show
  end

  scope "/", GalleyWeb do
    pipe_through :browser

    get "/", PageController, :index
 end

  scope "/", GalleyWeb do
    pipe_through [:browser, :require_authenticated_user]

    resources "/albums", AlbumController do
      resources "/photos", PhotoController, only: [:index, :new, :create]
      get "/photos/bulk_upload", PhotoController, :bulk_upload
    end
    resources "/photos", PhotoController, except: [:index, :new, :create]
    get "/photos/:id/raw", PhotoController, :raw
    get "/photos/:id/thumb", PhotoController, :thumb
  end

  scope "/admin", GalleyWeb.Admin, as: :admin do
    pipe_through [:browser, :require_admin]

    resources "/users", UserController
    resources "/invites", InviteController

    live_dashboard "/dashboard", metrics: GalleyWeb.Telemetry
  end

  scope "/ajax", GalleyWeb do
     pipe_through [:ajax, :require_authenticated_user]

     get "/session", AjaxSessionController, :show
     resources "/comments", CommentController, except: [:new, :edit]
     post "/upload/:hash", UploadController, :upload
  end

  # Other scopes may use custom stacks.
  # scope "/api", GalleyWeb do
  #   pipe_through :api
  # end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", GalleyWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", GalleyWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings", UserSettingsController, :update
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email
  end

  scope "/", GalleyWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :edit
    post "/users/confirm/:token", UserConfirmationController, :update
  end
end
