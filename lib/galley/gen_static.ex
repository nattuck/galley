defmodule Galley.GenStatic do

  alias Galley.Users
  alias Galley.Users.User
  alias Galley.Albums
  alias Galley.Albums.Album
  alias Galley.Photos.Photo
  alias GalleyWeb.PhotoView

  def gen_base_path do
    Path.expand("~/.local/data/galley/g")
  end

  def path_from_slugs(user_slug, album_slug) do
    gen_base_path()
    |> Path.join(user_slug)
    |> Path.join(album_slug)
  end

  def user_base_path(%User{} = user) do
    gen_base_path()
    |> Path.join(user.slug)
  end

  def user_base_path(slug) when is_binary(slug) do
    gen_base_path()
    |> Path.join(slug)
  end

  def user_base_path(user_id) do
    Users.get_user!(user_id)
    |> user_base_path()
  end

  def album_base_path(%Album{} = album) do
    case album.user do
      %User{} = user ->
        user_base_path(user)
      _else ->
        user_base_path(album.user_id)
    end
    |> Path.join(album.slug)
  end

  def album_base_path(album_id) do
    Albums.get_album!(album_id)
    |> album_base_path()
  end

  def gen_album!(%Album{} = album) do
    #IO.puts "Generating album #{album.id}"

    base = album_base_path(album)
    File.mkdir_p!(base)

    path = Path.join(base, "index.html")
    json = Jason.encode!(Map.take(album, [:name, :slug]), pretty: true)
    text = Phoenix.View.render_to_string(GalleyWeb.GenView, "index.html",
      layout: {GalleyWeb.LayoutView, "gen.html"}, album: album,
      album_json: json)
    File.write!(path, text)

    Enum.each album.photos, fn photo ->
      gen_photo_show!(album, photo)
    end

    link_shared_statics!(base)
    link_photos!(album, base)
    #regen_thumbs!(album)
    gen_user!(album.user_id)
    make_zip!(album)
  end

  def gen_album!(album_id) do
    Albums.get_album_for_static_gen!(album_id)
    |> gen_album!
  end

  def gen_photo_show!(%Album{} = album, %Photo{} = photo) do
    base = album_base_path(album)
    name = Photo.photo_page_name(photo)
    path = Path.join(base, name)
    text = Phoenix.View.render_to_string(GalleyWeb.GenView, "show.html",
      layout: {GalleyWeb.LayoutView, "gen.html"}, album: album, photo: photo,
      json: Jason.encode!(PhotoView.to_map(photo)))
    File.write!(path, text)
  end

  def gen_user!(%User{} = user) do
    user = Users.preload_for_static_gen!(user)
    base = user_base_path(user)
    path = Path.join(base, "index.html")
    text = Phoenix.View.render_to_string(GalleyWeb.GenView, "user.html",
      layout: {GalleyWeb.LayoutView, "gen.html"}, user: user)
    File.write!(path, text)

    link_shared_statics!(base)
  end

  def gen_user!(user_id) do
    Users.get_user!(user_id)
    |> gen_user!()
  end

  def schedule_gen_album(album_id) do
    if Application.get_env(:galley, Galley.GenStatic)[:async] do
      Task.start fn ->
        gen_album!(album_id)
      end
    else
      # Completely skip this in test mode.
      #gen_album!(album_id)
    end
  end

  def assets_path do
    Application.app_dir(:galley, "priv/static/assets")
  end

  def link_shared_statics!(path) do
    link_from_assets!("app.js", path, "galley.js")
    link_from_assets!("app.css", path, "galley.css")
  end

  def link_from_assets!(src, album_path, dst) do
    src_path = Path.join(assets_path(), src)
    dst_path = Path.join(album_path, dst)
    ln(src_path, dst_path)
 end

  def link_photos!(%Album{} = album, base) do
    images = Path.join(base, "images")
    thumbs = Path.join(base, "thumbs")
    File.mkdir_p!(images)
    File.mkdir_p!(thumbs)

    Enum.each album.photos, fn photo ->
      name = Photo.photo_name(photo)
      src = Photo.photo_path(photo)
      dst = Path.join(images, name)
      ln(src, dst)

      tsrc = Photo.thumb_path(photo)
      unless File.exists?(tsrc) do
        Photo.make_thumb!(photo)
      end
      tdst = Path.join(thumbs, name)
      ln(tsrc, tdst)
    end
  end

  def ln(src, dst) do
    if String.length(dst) < 10 do
      raise "Don't trust dst path"
    end

    File.rm(dst)
    File.ln_s(src, dst)
  end

  def regen_thumbs!(album) do
    Enum.each album.photos, fn photo ->
      Photo.make_thumb!(photo)
    end
  end

  def make_zip!(%Album{} = album) do
    album = Albums.preload_for_static_gen!(album)

    script = Application.app_dir(:galley, "priv/scripts/zip-album.sh")
    base = user_base_path(album.user)
    name = album.slug

    {text, 0} = System.cmd("bash", [script, base, name])
    IO.puts(text)
    :ok
  end

  def make_zip!(album_id) do
    Albums.get_album!(album_id)
    |> make_zip!()
  end
end
