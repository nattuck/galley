defmodule Galley.Albums do
  @moduledoc """
  The Albums context.
  """

  import Ecto.Query, warn: false
  alias Galley.Repo

  alias Galley.Albums.Album
  alias Galley.Photos
  alias Galley.Users
  alias Galley.GenStatic

  @doc """
  Returns the list of albums.

  ## Examples

      iex> list_albums()
      [%Album{}, ...]

  """
  def list_albums do
    Repo.all(Album)
    |> Repo.preload(:user)
  end

  def list_albums_for_user(user) do
    Repo.all from al in Album,
      where: al.user_id == ^user.id,
      preload: [:user]
  end

  @doc """
  Gets a single album.

  Raises `Ecto.NoResultsError` if the Album does not exist.

  ## Examples

      iex> get_album!(123)
      %Album{}

      iex> get_album!(456)
      ** (Ecto.NoResultsError)

  """
  def get_album!(id), do: Repo.get!(Album, id)

  def get_album_and_contents!(id) do
    Repo.one! from al in Album,
      where: al.id == ^id,
      preload: [:user, :photos]
  end

  def get_album_by_slugs!(uslug, aslug) do
    user = Users.get_user_by_slug!(uslug)
    album = Repo.one! from al in Album,
      where: al.slug == ^aslug and al.user_id == ^user.id,
      preload: [:photos]
    %Album{ album | user: user }
  end

  def get_album_for_static_gen!(id) do
    Repo.one! from al in Album,
      where: al.id == ^id,
      preload: [:user, photos: [comments: :user]]
  end

  def preload_for_static_gen!(%Album{} = album) do
    Repo.preload(album, [:user, photos: [comments: :user]])
  end

  def load_user(album) do
    Repo.preload(album, :user)
  end

  @doc """
  Creates a album.

  ## Examples

      iex> create_album(%{field: value})
      {:ok, %Album{}}

      iex> create_album(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_album(attrs \\ %{}) do
    %Album{}
    |> Album.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a album.

  ## Examples

      iex> update_album(album, %{field: new_value})
      {:ok, %Album{}}

      iex> update_album(album, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_album(%Album{} = album, attrs) do
    album
    |> Album.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a album.

  ## Examples

      iex> delete_album(album)
      {:ok, %Album{}}

      iex> delete_album(album)
      {:error, %Ecto.Changeset{}}

  """
  def delete_album(%Album{} = album) do
    Repo.delete(album)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking album changes.

  ## Examples

      iex> change_album(album)
      %Ecto.Changeset{data: %Album{}}

  """
  def change_album(%Album{} = album, attrs \\ %{}) do
    Album.changeset(album, attrs)
  end

  def next_album_order(album, offset) do
    next_album_order(album) + 10*offset
  end

  def next_album_order(%Album{} = album) do
    album = Repo.preload(album, :photos)
    top = album.photos
    |> Enum.map(&(&1.order))
    |> Enum.max(fn -> 0 end)
    top + 10
  end
  def next_album_order(album_id) do
    get_album!(album_id)
    |> next_album_order()
  end

  def shift_album_photo(album_id, photo_id, dx) do
    album = Repo.one! from al in Album,
      where: al.id == ^album_id,
      preload: [:photos]
    photos = Enum.sort_by(album.photos, &(&1.order))

    if length(photos) > 1 do
      photo = Enum.find(photos, &(&1.id == photo_id))
      p_idx = Enum.find_index(photos, &(&1.id == photo_id))

      o_idx = Integer.mod(p_idx + sign(dx), length(photos))
      other = Enum.at(photos, o_idx)

      {:ok, _} = Photos.swap_photo_orders(photo, other)

      GenStatic.schedule_gen_album(album.id)
      {:ok, get_album_and_contents!(album.id)}
    else
      {:ok, album}
    end
  end

  def sign(xx) when xx < 0, do: -1
  def sign(_x), do: 1
end
