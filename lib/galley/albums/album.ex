defmodule Galley.Albums.Album do
  use Ecto.Schema
  import Ecto.Changeset

  schema "albums" do
    field :desc, :string
    field :name, :string
    field :slug, :string

    belongs_to :user, Galley.Users.User
    has_many :photos, Galley.Photos.Photo

    timestamps()
  end

  @doc false
  def changeset(album, attrs) do
    album
    |> cast(attrs, [:name, :slug, :desc, :user_id])
    |> validate_required([:name, :slug, :user_id])
  end
end
