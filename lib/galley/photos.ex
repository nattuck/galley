defmodule Galley.Photos do
  @moduledoc """
  The Photos context.
  """

  import Ecto.Query, warn: false
  alias Galley.Repo

  alias Galley.Photos.Photo
  alias Galley.Albums
  alias Galley.GenStatic

  @doc """
  Returns the list of photos.

  ## Examples

      iex> list_photos()
      [%Photo{}, ...]

  """
  def list_photos(album_id) do
    Repo.all from ph in Photo,
      where: ph.album_id == ^album_id
  end

  def list_photos() do
    Repo.all(Photo)
  end

  @doc """
  Gets a single photo.

  Raises `Ecto.NoResultsError` if the Photo does not exist.

  ## Examples

      iex> get_photo!(123)
      %Photo{}

      iex> get_photo!(456)
      ** (Ecto.NoResultsError)

  """
  def get_photo!(id), do: Repo.get!(Photo, id)

  def get_photos_by_hash(hash) do
    Repo.all from ph in Photo,
      where: ph.hash == ^hash
  end

  def get_photo_by_album_order!(album_id, order) do
    Repo.one! from ph in Photo,
      where: ph.album_id == ^album_id and ph.order == ^order
  end

  @doc """
  Creates a photo.

  ## Examples

      iex> create_photo(%{field: value})
      {:ok, %Photo{}}

      iex> create_photo(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_photo(attrs \\ %{}) do
    attrs = attrs
    |> Map.merge(Photo.inspect_photo!(attrs["photo"]))

    rv = %Photo{}
    |> Photo.changeset(attrs)
    |> Repo.insert()

    case rv do
      {:ok, photo} ->
        Task.start fn ->
          Photo.write_photo!(photo, attrs["data"])
          Photo.make_thumb!(photo)
        end
        {:ok, photo}
      other ->
        other
    end

#    rv = Ecto.Multi.new()
#    |> Ecto.Multi.insert(:photo, fn %{} ->
#      Photo.changeset(%Photo{}, attrs)
#    end)
#    |> Ecto.Multi.run(:write, fn _repo, %{photo: photo} ->
#      Photo.write_photo!(photo, attrs["data"])
#      Photo.make_thumb!(photo)
#    end)
#    |> Repo.transaction()
#
#    case rv do
#      {:ok, %{photo: photo}} ->
#        #GenStatic.schedule_gen_album(photo.album_id)
#        {:ok, photo}
#      {:error, :photo, cset, _} ->
#        {:error, cset}
#    end
  end

  @doc """
  Updates a photo.

  ## Examples

      iex> update_photo(photo, %{field: new_value})
      {:ok, %Photo{}}

      iex> update_photo(photo, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_photo(%Photo{} = photo, attrs) do
    rv = photo
    |> Photo.changeset(attrs)
    |> Repo.update()

    case rv do
      {:ok, photo} ->
        GenStatic.schedule_gen_album(photo.album_id)
        {:ok, photo}
      {:error, %Ecto.Changeset{} = cset} ->
        case Keyword.get(cset.errors, :order) do
          {"has already been taken", _} ->
            order = attrs["order"] || attrs[:order]
            photo2 = get_photo_by_album_order!(photo.album_id, order)
            {:ok, rv} = swap_photo_orders(photo, photo2)
            GenStatic.schedule_gen_album(photo.album_id)
            {:ok, rv[:ph1a]}
          _else ->
            {:error, cset}
        end
    end
  end

  def update_photo(attrs) do
    # FIXME: Make this a transaction.
    get_photo!(attrs["id"])
    |> update_photo(attrs)
  end

  def swap_photo_orders(%Photo{} = p1, %Photo{} = p2) do
    unless p1.album_id == p2.album_id do
      raise "Photos must be in same album."
    end

    temp_order = Albums.next_album_order(p1.album_id)

    Ecto.Multi.new()
    |> Ecto.Multi.run(:ph1, fn _, _ ->
      case Repo.get(Photo, p1.id) do
        nil -> {:error, :not_found}
        photo -> {:ok, photo}
      end
    end)
    |> Ecto.Multi.run(:ph2, fn _, _ ->
      case Repo.get(Photo, p2.id) do
        nil -> {:error, :not_found}
        photo -> {:ok, photo}
      end
    end)
    |> Ecto.Multi.update(:ph1t, fn %{ph1: ph1} ->
      Ecto.Changeset.change(ph1, order: temp_order)
    end)
    |> Ecto.Multi.update(:ph2a, fn %{ph1: ph1, ph2: ph2} ->
      Ecto.Changeset.change(ph2, order: ph1.order)
    end)
    |> Ecto.Multi.update(:ph1a, fn %{ph1: ph1, ph2: ph2} ->
      Ecto.Changeset.change(ph1, order: ph2.order)
    end)
    |> Repo.transaction()
  end

  @doc """
  Deletes a photo.

  ## Examples

      iex> delete_photo(photo)
      {:ok, %Photo{}}

      iex> delete_photo(photo)
      {:error, %Ecto.Changeset{}}

  """
  def delete_photo(%Photo{} = photo) do
    Repo.delete(photo)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking photo changes.

  ## Examples

      iex> change_photo(photo)
      %Ecto.Changeset{data: %Photo{}}

  """
  def change_photo(%Photo{} = photo, attrs \\ %{}) do
    Photo.changeset(photo, attrs)
  end

  def cleanup_orphan_files() do
    Enum.each Photo.list_file_hashes(), fn hash ->
      ps = get_photos_by_hash(hash)
      if Enum.empty?(ps) do
        Photo.photo_path(hash)
        |> remove_file()

        Photo.thumb_path(hash)
        |> remove_file()
      end
    end
  end

  def remove_file(path) when is_binary(path) do
    if String.length(path) > 10 && Regex.match?(~r/\.jpg$/i, path) do
      IO.puts "cleanup orphan: #{path}"
      File.rm(path)
    end
  end
end
