defmodule Galley.Comments.Comment do
  use Ecto.Schema
  import Ecto.Changeset

  @timestamps_opts [
    type: :utc_datetime_usec, autogenerate: {DateTime, :now!, ["Etc/UTC"]}
  ]

  schema "comments" do
    field :body, :string
    field :approved_at, :utc_datetime_usec
    belongs_to :user, Galley.Users.User
    belongs_to :photo, Galley.Photos.Photo

    timestamps()
  end

  @doc false
  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [:body, :user_id, :photo_id])
    |> validate_required([:body, :user_id, :photo_id])
  end
end
