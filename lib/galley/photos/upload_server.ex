defmodule Galley.Photos.UploadServer do
  # Upload server stores info about active uploads.
  # It's really just a global key-value map.

  # Contents:
  #  - key is photo hash (Sha256, lowercase hex, truncated to 48 bytes)
  #  - value is %{ hash, name, size, type, album_id, user_id, time }
  #  - value has string keys

  use GenServer

  ## External interface
  def start_link(_) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def put(key, val) do
    GenServer.call(__MODULE__, {:put, key, val})
  end

  def get(key) do
    GenServer.call(__MODULE__, {:get, key})
  end

  def del(key) do
    GenServer.call(__MODULE__, {:del, key})
  end

  ## GenServer callbacks

  @impl true
  def init(map) do
    {:ok, map}
  end

  @impl true
  def handle_call({:put, key, val}, _from, state0) do
    val = Map.put(val, "time", unix_now())
    state1 = Map.put(state0, key, val)
    {:reply, :ok, drop_expired(state1)}
  end

  def handle_call({:get, key}, _from, state) do
    {:reply, {:ok, Map.get(state, key)}, state}
  end

  def handle_call({:del, key}, _from, state) do
    {:reply, :ok, Map.delete(state, key)}
  end

  def drop_expired(mm) do
    now = unix_now()
    Enum.filter(mm, fn {_kk, vv} ->
      age = now - vv["time"]
      age < (2 * 60 * 60)
    end)
    |> Enum.into(%{})
  end

  def unix_now() do
    DateTime.utc_now()
    |> DateTime.to_unix()
  end
end
