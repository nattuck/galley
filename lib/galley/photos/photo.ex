defmodule Galley.Photos.Photo do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__

  schema "photos" do
    field :bytes, :integer
    field :filename, :string
    field :hash, :string
    field :caption, :string, default: ""
    field :order, :integer, default: 10

    belongs_to :album, Galley.Albums.Album
    has_many :comments, Galley.Comments.Comment

    timestamps()
  end

  @doc false
  def changeset(photo, attrs) do
    photo
    |> cast(attrs, [:caption, :order, :filename, :hash, :bytes, :album_id])
    |> validate_required([:filename, :hash, :bytes, :album_id])
    |> unique_constraint(:order, name: :photos_album_id_order_index)
    |> unique_constraint(:filename, name: :photos_album_id_filename_index)
    |> unique_constraint(:hash, name: :photos_album_id_hash_index)
  end

  #
  # Handle actual photo files.
  #

  def data_hash(data) do
    :crypto.hash(:sha256, data)
    |> Base.encode16(case: :lower)
    |> String.slice(0, 48)
  end

  def file_hash!(path) do
    File.read!(path)
    |> data_hash()
  end

  def file_size!(path) do
    stat = File.stat!(path)
    stat.size
  end

  def inspect_photo!(nil), do: %{}
  def inspect_photo!(%Plug.Upload{filename: filename, path: path}) do
    data = File.read!(path)
    inspect_photo!(%{name: filename, data: data})
 end
  def inspect_photo!(%{name: name, data: data}) when is_binary(data) do
    %{
      "filename" => name,
      "hash" => data_hash(data),
      "bytes" => byte_size(data),
      "data" => data,
    }
  end

  def photo_base_path() do
    "~/.local/data/galley/photos"
    |> Path.expand()
  end

  def list_photo_hashes() do
    photo_base_path()
    |> File.ls!()
    |> Enum.map(&hash_from_path/1)
  end

  def thumb_base_path() do
    "~/.local/data/galley/thumbs"
    |> Path.expand()
  end

  def list_thumb_hashes() do
    photo_base_path()
    |> File.ls!()
    |> Enum.map(&hash_from_path/1)
  end

  def list_file_hashes() do
    xs = list_photo_hashes()
    ys = list_thumb_hashes()
    Enum.uniq(xs ++ ys)
  end

  def hash_name(%Photo{} = photo) do
    "#{photo.hash}.jpg"
  end

  def hash_name(hash) when is_binary(hash) do
    "#{hash}.jpg"
  end

  def photo_name(%Photo{} = photo) do
    name = photo.order
    |> Integer.to_string()
    |> String.pad_leading(4, "0")
    "#{name}.jpg"
  end

  def photo_page_name(%Photo{} = photo) do
    name = photo.order
    |> Integer.to_string()
    |> String.pad_leading(4, "0")
    "#{name}.html"
  end

  def photo_path(photo) do
    base = photo_base_path()
    name = hash_name(photo)
    Path.join(base, name)
  end

  def thumb_path(photo) do
    base = thumb_base_path()
    name = hash_name(photo)
    Path.join(base, name)
  end

  def hash_from_path(path) do
    [_, hash] = Regex.run(~r/(\w+)\.jpg$/, path)
    hash
  end

  def write_photo!(photo, nil) do
    if File.exists?(photo_path(photo)) do
      {:ok, nil}
    else
      raise "No photo to write"
    end
  end
  def write_photo!(%Photo{} = photo, data) do
    File.mkdir_p!(photo_base_path())
    path = photo_path(photo)
    File.write!(path, data)
    {:ok, nil}
  end

  def make_thumb!(%Photo{} = photo) do
    path = photo_path(photo)
    make_thumb!(path)
  end

  def make_thumb!(path) do
    File.mkdir_p!(thumb_base_path())
    hash = hash_from_path(path)

    {ww, hh} = photo_dims(path)

    crop = if ww > hh do
      #IO.puts "#{photo.filename} wide"
      # wide
      dd = floor((ww - hh)/2)
      "#{hh}x#{hh}+#{dd}+0"
    else
      #IO.puts "#{photo.filename} tall"
      # tall
      dd = floor((hh - ww)/2)
      "#{ww}x#{ww}+0+#{dd}"
    end

    unless File.exists?(path) do
      raise "No such file: '#{path}'"
    end

    unless File.exists?(thumb_path(hash)) do
      {_, 0} = System.cmd(
        "convert",
        [
          path,
          "-crop", crop,
          "-resize", "300x300",
          thumb_path(hash)
        ]
      )
    end

    {:ok, nil}
  end

  def photo_dims(photo) do
    geo = read_meta(photo)
    |> Map.get("image")
    |> Map.get("geometry")
    ww = geo["width"]
    hh = geo["height"]
    {ww, hh}
  end

  def read_meta(%Photo{} = photo) do
    read_meta(photo_path(photo))
  end
  def read_meta(path) do
    unless File.exists?(path) do
      raise "No such file: '#{path}'"
    end
    {text, 0} = System.cmd("convert", [path, "json:"])
    Jason.decode!(text) |> hd
  end
end
