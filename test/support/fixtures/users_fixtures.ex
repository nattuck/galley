defmodule Galley.UsersFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Galley.Users` context.
  """

  def unique_user_name, do: "User#{System.unique_integer()} McUser"
  def unique_user_email, do: "user#{System.unique_integer()}@example.com"
  def valid_user_password, do: "hello world!"

  def valid_user_attributes(attrs \\ %{}) do
    name = unique_user_name()

    Enum.into(attrs, %{
      name: name,
      slug: Galley.Users.User.name_to_slug(name),
      email: unique_user_email(),
      password: valid_user_password(),
      admin: false,
    })
  end

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> valid_user_attributes()
      |> Galley.Users.create_user()

    user
  end

  def admin_fixture() do
    user_fixture(%{admin: true})
  end

  def extract_user_token(fun) do
    {:ok, captured_email} = fun.(&"[TOKEN]#{&1}[TOKEN]")
    [_, token | _] = String.split(captured_email.text_body, "[TOKEN]")
    token
  end
end
