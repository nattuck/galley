defmodule Galley.AlbumsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Galley.Albums` context.
  """

  import Galley.UsersFixtures

  @doc """
  Generate a album.
  """
  def album_fixture(attrs \\ %{}) do
    user = user_fixture()

    {:ok, album} =
      attrs
      |> Enum.into(%{
        desc: "some desc",
        name: "some name",
        slug: "some slug",
        user_id: user.id
      })
      |> Galley.Albums.create_album()

    album
  end
end
