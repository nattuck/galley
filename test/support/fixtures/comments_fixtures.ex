defmodule Galley.CommentsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Galley.Comments` context.
  """

  import Galley.UsersFixtures
  import Galley.PhotosFixtures

  @doc """
  Generate a comment.
  """
  def comment_fixture(attrs \\ %{}) do
    user = user_fixture()
    photo = photo_fixture()

    {:ok, comment} =
      attrs
      |> Enum.into(%{
          body: "some body",
          user_id: user.id,
          photo_id: photo.id,
      })
      |> Galley.Comments.create_comment()

    comment
  end
end
