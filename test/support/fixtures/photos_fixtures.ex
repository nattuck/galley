defmodule Galley.PhotosFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Galley.Photos` context.
  """

  import Galley.AlbumsFixtures

  @doc """
  Generate a photo.
  """
  def photo_fixture(attrs \\ %{}) do
    album = album_fixture()
    upload = upload_fixture()

    {:ok, photo} =
      attrs
      |> Enum.into(%{
        "album_id" => album.id,
        "caption" => "some caption",
        "order" => 42,
        "photo" => upload,
      })
      |> Galley.Photos.create_photo()

    photo
  end

  def upload_fixture() do
    Application.app_dir(:galley, "priv/testdata/helsinki.jpg")
    |> upload_fixture()
  end

  def upload_fixture(path) do
    %Plug.Upload{
      content_type: "image/jpeg",
      filename: Path.basename(path),
      path: path,
    }
  end
end
