defmodule Galley.InvitesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Galley.Invites` context.
  """

  @doc """
  Generate a invite.
  """
  def invite_fixture(attrs \\ %{}) do
    {:ok, invite} =
      attrs
      |> Enum.into(%{
        email: "some.email@example.com"
      })
      |> Galley.Invites.create_invite()

    invite
  end
end
