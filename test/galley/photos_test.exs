defmodule Galley.PhotosTest do
  use Galley.DataCase

  alias Galley.Photos
  alias Galley.Photos.Photo

  import Galley.PhotosFixtures
  import Galley.AlbumsFixtures

  describe "photos" do
    @invalid_attrs %{bytes: nil, filename: nil, hash: nil}

    test "list_photos/0 returns all photos" do
      photo = photo_fixture()
      assert Photos.list_photos(photo.album_id) == [photo]
    end

    test "get_photo!/1 returns the photo with given id" do
      photo = photo_fixture()
      assert Photos.get_photo!(photo.id) == photo
    end

    test "create_photo/1 with valid data creates a photo" do
      album = album_fixture()
      upload = upload_fixture()
      valid_attrs = %{
        "album_id" => album.id,
        "photo" => upload,
        "order" => 42,
      }

      assert {:ok, %Photo{} = photo} = Photos.create_photo(valid_attrs)
      assert photo.order == 42
    end

    test "create_photo/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Photos.create_photo(@invalid_attrs)
    end

    test "update_photo/2 with valid data updates the photo" do
      photo = photo_fixture()
      update_attrs = %{caption: "some new caption"}

      assert {:ok, %Photo{} = photo} = Photos.update_photo(photo, update_attrs)
      assert photo.caption == "some new caption"
    end

    test "update_photo/2 with invalid data returns error changeset" do
      photo = photo_fixture()
      assert {:error, %Ecto.Changeset{}} = Photos.update_photo(photo, @invalid_attrs)
      assert photo == Photos.get_photo!(photo.id)
    end

    test "delete_photo/1 deletes the photo" do
      photo = photo_fixture()
      assert {:ok, %Photo{}} = Photos.delete_photo(photo)
      assert_raise Ecto.NoResultsError, fn -> Photos.get_photo!(photo.id) end
    end

    test "change_photo/1 returns a photo changeset" do
      photo = photo_fixture()
      assert %Ecto.Changeset{} = Photos.change_photo(photo)
    end
  end
end
