defmodule Galley.AlbumsTest do
  use Galley.DataCase

  alias Galley.Albums
  alias Galley.Albums.Album

  import Galley.AlbumsFixtures
  import Galley.UsersFixtures

  describe "albums" do

    @invalid_attrs %{desc: nil, name: nil, slug: nil}

    test "list_albums/0 returns all albums" do
      album = album_fixture()
      assert Albums.list_albums() == [album]
    end

    test "get_album!/1 returns the album with given id" do
      album = album_fixture()
      assert Albums.get_album!(album.id) == album
    end

    test "create_album/1 with valid data creates a album" do
      user = user_fixture()
      valid_attrs = %{
        desc: "some desc", name: "some name",
        slug: "some-slug", user_id: user.id
      }

      assert {:ok, %Album{} = album} = Albums.create_album(valid_attrs)
      assert album.desc == "some desc"
      assert album.name == "some name"
      assert album.slug == "some-slug"
    end

    test "create_album/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Albums.create_album(@invalid_attrs)
    end

    test "update_album/2 with valid data updates the album" do
      album = album_fixture()
      update_attrs = %{desc: "some updated desc", name: "some updated name", slug: "some updated slug"}

      assert {:ok, %Album{} = album} = Albums.update_album(album, update_attrs)
      assert album.desc == "some updated desc"
      assert album.name == "some updated name"
      assert album.slug == "some updated slug"
    end

    test "update_album/2 with invalid data returns error changeset" do
      album = album_fixture()
      assert {:error, %Ecto.Changeset{}} = Albums.update_album(album, @invalid_attrs)
      assert album == Albums.get_album!(album.id)
    end

    test "delete_album/1 deletes the album" do
      album = album_fixture()
      assert {:ok, %Album{}} = Albums.delete_album(album)
      assert_raise Ecto.NoResultsError, fn -> Albums.get_album!(album.id) end
    end

    test "change_album/1 returns a album changeset" do
      album = album_fixture()
      assert %Ecto.Changeset{} = Albums.change_album(album)
    end
  end
end
