defmodule GalleyWeb.PageControllerTest do
  use GalleyWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Public Albums"
  end
end
