defmodule GalleyWeb.PhotoControllerTest do
  use GalleyWeb.ConnCase

  import Galley.PhotosFixtures
  import Galley.AlbumsFixtures

  @create_attrs %{
    bytes: 42, filename: "some filename", hash: "some hash",
    caption: "some caption", order: 43,
  }
  @invalid_attrs %{
    bytes: nil, filename: nil, hash: nil, caption: nil, order: -1
  }

  describe "index" do
    setup [:register_and_log_in_user]

    test "lists all photos for album", %{conn: conn} do
      photo = photo_fixture()

      conn = get(conn, Routes.album_photo_path(conn, :index, photo.album_id))
      assert html_response(conn, 200) =~ "Listing Photos"
    end
  end

  describe "new photo" do
    setup [:register_and_log_in_user]

    test "renders form", %{conn: conn} do
      album = album_fixture()
      conn = get(conn, Routes.album_photo_path(conn, :new, album))
      assert html_response(conn, 200) =~ "Add Photo"
    end
  end

  describe "create photo" do
    setup [:register_and_log_in_user]

    test "redirects to show when data is valid", %{conn: conn} do
      album = album_fixture()
      conn = post(conn, Routes.album_photo_path(conn, :create, album),
        photo: Map.put(@create_attrs, "photo", upload_fixture()))

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.album_path(conn, :show, album)

      conn = get(conn, Routes.photo_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Photo"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      album = album_fixture()
      conn = post(conn, Routes.album_photo_path(conn, :create, album),
        photo: @invalid_attrs)
      assert html_response(conn, 200) =~ "Add Photo"
    end
  end

  describe "edit photo" do
    setup [:create_photo, :register_and_log_in_user]

    test "renders form for editing chosen photo", %{conn: conn, photo: photo} do
      conn = get(conn, Routes.photo_path(conn, :edit, photo))
      assert html_response(conn, 200) =~ "Edit Photo"
    end
  end

  describe "update photo" do
    setup [:create_photo, :register_and_log_in_user]

    test "redirects when data is valid", %{conn: conn, photo: photo} do
      conn = put(conn, Routes.photo_path(conn, :update, photo),
        photo: %{caption: "updated caption"})
      assert redirected_to(conn) == Routes.photo_path(conn, :show, photo)

      conn = get(conn, Routes.photo_path(conn, :show, photo))
      assert html_response(conn, 200) =~ "updated caption"
    end

    test "renders errors when data is invalid", %{conn: conn, photo: photo} do
      conn = put(conn, Routes.photo_path(conn, :update, photo), photo: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Photo"
    end
  end

  describe "delete photo" do
    setup [:create_photo, :register_and_log_in_user]

    test "deletes chosen photo", %{conn: conn, photo: photo} do
      conn = delete(conn, Routes.photo_path(conn, :delete, photo))
      assert redirected_to(conn) == Routes.album_photo_path(conn, :index, photo.album_id)

      assert_error_sent 404, fn ->
        get(conn, Routes.photo_path(conn, :show, photo))
      end
    end
  end

  defp create_photo(_) do
    photo = photo_fixture()
    %{photo: photo}
  end
end
