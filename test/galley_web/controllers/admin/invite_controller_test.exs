defmodule GalleyWeb.Admin.InviteControllerTest do
  use GalleyWeb.ConnCase

  import Galley.InvitesFixtures

  @create_attrs %{email: "some.email@example.com"}
  @update_attrs %{email: "updated@example.com"}
  @invalid_attrs %{email: "has spaces"}

  describe "index" do
    setup [:register_and_log_in_admin]

    test "lists all invites", %{conn: conn} do
      conn = get(conn, Routes.admin_invite_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Invites"
    end
  end

  describe "new invite" do
    setup [:register_and_log_in_admin]

    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.admin_invite_path(conn, :new))
      assert html_response(conn, 200) =~ "New Invite"
    end
  end

  describe "create invite" do
    setup [:register_and_log_in_admin]

    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.admin_invite_path(conn, :create), invite: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.admin_invite_path(conn, :show, id)

      conn = get(conn, Routes.admin_invite_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Invite"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.admin_invite_path(conn, :create), invite: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Invite"
    end
  end

  describe "edit invite" do
    setup [:register_and_log_in_admin, :create_invite]

    test "renders form for editing chosen invite", %{conn: conn, invite: invite} do
      conn = get(conn, Routes.admin_invite_path(conn, :edit, invite))
      assert html_response(conn, 200) =~ "Edit Invite"
    end
  end

  describe "update invite" do
    setup [:register_and_log_in_admin, :create_invite]

    test "redirects when data is valid", %{conn: conn, invite: invite} do
      conn = put(conn, Routes.admin_invite_path(conn, :update, invite), invite: @update_attrs)
      assert redirected_to(conn) == Routes.admin_invite_path(conn, :show, invite)

      conn = get(conn, Routes.admin_invite_path(conn, :show, invite))
      assert html_response(conn, 200) =~ "updated@example.com"
    end

    test "renders errors when data is invalid", %{conn: conn, invite: invite} do
      conn = put(conn, Routes.admin_invite_path(conn, :update, invite), invite: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Invite"
    end
  end

  describe "delete invite" do
    setup [:register_and_log_in_admin, :create_invite]

    test "deletes chosen invite", %{conn: conn, invite: invite} do
      conn = delete(conn, Routes.admin_invite_path(conn, :delete, invite))
      assert redirected_to(conn) == Routes.admin_invite_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.admin_invite_path(conn, :show, invite))
      end
    end
  end

  defp create_invite(_) do
    invite = invite_fixture()
    %{invite: invite}
  end
end
