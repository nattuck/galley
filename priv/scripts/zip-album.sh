#!/bin/bash

BASE=$1
NAME=$2

if [ ! -d "$BASE" ]; then
    echo "Base dir must exist".
    exit 1
fi

if [ ! -d "$BASE/$NAME" ]; then
    echo "Album dir must exist."
    exit 1
fi

cd "$BASE" || exit

ZIP="$NAME.zip"

pwd
zip -r -FS "$ZIP" "$NAME"
