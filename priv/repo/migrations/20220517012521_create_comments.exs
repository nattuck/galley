defmodule Galley.Repo.Migrations.CreateComments do
  use Ecto.Migration

  def change do
    create table(:comments) do
      add :body, :text, null: false
      add :approved_at, :utc_datetime_usec
      add :user_id, references(:users, on_delete: :nothing), null: false
      add :photo_id, references(:photos, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:comments, [:user_id])
    create index(:comments, [:photo_id])
  end
end
