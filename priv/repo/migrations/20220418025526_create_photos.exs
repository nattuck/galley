defmodule Galley.Repo.Migrations.CreatePhotos do
  use Ecto.Migration

  def change do
    create table(:photos) do
      add :filename, :string, null: false
      add :hash, :string, null: false
      add :bytes, :integer, null: false
      add :album_id, references(:albums, on_delete: :delete_all), null: false
      add :order, :integer, null: false, default: 10
      add :caption, :text, null: false, default: ""

      timestamps()
    end

    create index(:photos, [:album_id])
    create index(:photos, [:hash])
    create unique_index(:photos, [:album_id, :order])
    create unique_index(:photos, [:album_id, :hash])
    create unique_index(:photos, [:album_id, :filename])
  end
end
