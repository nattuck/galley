defmodule Galley.Repo.Migrations.CreateAlbums do
  use Ecto.Migration

  def change do
    create table(:albums) do
      add :name, :string, null: false
      add :slug, :string, null: false
      add :desc, :text, null: false, default: ""
      add :user_id, references(:users, on_delete: :restrict), null: false

      timestamps()
    end

    create index(:albums, [:user_id])
    create index(:albums, [:slug])
    create unique_index(:albums, [:user_id, :slug])
  end
end
