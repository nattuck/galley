defmodule Galley.Repo.Migrations.CreateInvites do
  use Ecto.Migration

  def change do
    create table(:invites) do
      add :email, :string, null: false

      timestamps()
    end

    create unique_index(:invites, [:email])
  end
end
