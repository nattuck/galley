# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Galley.Repo.insert!(%Galley.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

#alias Galley.Repo
alias Galley.Users

attrs = %{
  "name" => "Nat Tuck", "slug" => "nat", "password" => "goatgoatgoat",
  "email" => "nat@ferrus.net"
}
Users.register_user(attrs)
